
import com.rapidminer.Process;
import com.rapidminer.RapidMiner;
import com.rapidminer.example.Attribute;
import com.rapidminer.example.Attributes;
import com.rapidminer.example.ExampleSet;
import com.rapidminer.operator.IOContainer;
import com.rapidminer.operator.Operator;
import com.rapidminer.operator.OperatorException;
import com.rapidminer.operator.io.ExcelExampleSource;
import com.rapidminer.tools.XMLException;
import java.io.File;
import java.io.IOException;
import java.lang.Object;
import com.rapidminer.tools.jdbc.JDBCProperties;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.util.Iterator;
import javax.swing.*;
import gate.swing.SpringUtilities;
import java.awt.event.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class ReadRapidminerProcess {

    private JTextArea textArea;
    private JLabel labelRes;
    private JFrame frame;
    private JPanel p;
    private JScrollPane scrollPane;
    private String path;
    private JTextField textField;
    private String old;
    
    private void createAndShowGUI() {
        
        p = new JPanel(new SpringLayout());
        JLabel l = new JLabel("Enter your CSV file path:");
        textField = new JTextField();
        JButton button = new JButton("Submit");
        
        p.add(l);
        p.add(textField);
        p.add(button);
        

        // Lay out the panel.
        SpringUtilities.makeCompactGrid(p, 3, 1, // rows, cols
                17, 17, // initX, initY
                17, 17); // xPad, yPad

        button.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                path = textField.getText();

                if(path == null){
                    JOptionPane.showMessageDialog(null,"Must fill input!","Error",JOptionPane.ERROR_MESSAGE);
                }
                else{
                    try {
                        try {
                            try {
                                changePath(path);
                            } catch (SAXException ex) {
                                Logger.getLogger(ReadRapidminerProcess.class.getName()).log(Level.SEVERE, null, ex);
                            } catch (XPathExpressionException ex) {
                                Logger.getLogger(ReadRapidminerProcess.class.getName()).log(Level.SEVERE, null, ex);
                            }
                        } catch (FileNotFoundException ex) {
                            Logger.getLogger(ReadRapidminerProcess.class.getName()).log(Level.SEVERE, null, ex);
                        } catch (ParserConfigurationException ex) {
                            Logger.getLogger(ReadRapidminerProcess.class.getName()).log(Level.SEVERE, null, ex);
                        }
                    } catch (IOException ex) {
                        Logger.getLogger(ReadRapidminerProcess.class.getName()).log(Level.SEVERE, null, ex);
                    }
                }
            }
        });
        // Create and set up the window.
        frame = new JFrame("MI-DDW, HW2");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // Set up the content pane.
        p.setOpaque(true); // content panes must be opaque
        frame.setContentPane(p);

        // Display the window.
        frame.pack();
        frame.setVisible(true);
        frame.setSize(300,200);
    }
    
    private void changePath(String filePath) throws FileNotFoundException, IOException, ParserConfigurationException, SAXException, XPathExpressionException {
        BufferedReader br = new BufferedReader(new FileReader("/Users/baji/skola/DDW/semestralka_rapid/data/read_csv_orig.rmp"));
        try {
            StringBuilder sb = new StringBuilder();
            String line = br.readLine();

            while (line != null) {
                sb.append(line);
                sb.append(System.lineSeparator());
                line = br.readLine();
                if(line != null){
                    if (line.equals("        <parameter key=\"csv_file\" value=\"\"/>")){
                        line = "        <parameter key=\"csv_file\" value=\""+filePath+"\"/>";
                    }
                }
            }
            String newF = sb.toString();
            PrintWriter writer = new PrintWriter("/Users/baji/skola/DDW/semestralka_rapid/data/read_csv.rmp", "UTF-8");
            writer.println(newF);
            writer.close();
        } finally {
            br.close();
        }
        run();
    }
    
    private void run() {
        try {
            RapidMiner.setExecutionMode(RapidMiner.ExecutionMode.COMMAND_LINE);
            RapidMiner.init();

            Process process = new Process(new File("/Users/baji/skola/DDW/semestralka_rapid/data/read_csv.rmp"));
            IOContainer c = process.run();
            PrintWriter writer = new PrintWriter("/Users/baji/skola/DDW/semestralka_rapid/output/outputFile.txt", "UTF-8");
            writer.println(c);
            writer.close();
            
        } catch (IOException | XMLException | OperatorException ex) {
            ex.printStackTrace();
        }
    }
    
    
    public static void main(String[] args) {
        ReadRapidminerProcess rp = new ReadRapidminerProcess();
        rp.createAndShowGUI();
    }
}